package dto;
import dto.MetodoAleatorio;

public abstract class Persona {
    //Los atributos
    private String nombre;
    private char sexo;
    private int edad;
    private boolean asistencia;
     
    //Hago las constantes que utilizare
    private final String[] NOMBRESCHICOS={"Pepe", "Fernando", "Alberto", "Nacho", "Eustaquio"}; 
    private final String[] NOMBRESCHICAS={"Alicia", "Laura", "Ana", "Pepa", "Elena"}; 
    private final int CHICO=0;
    private final int CHICA=1;
     
    //Hago el constructor
    public Persona(){
         
        //entre 0 y 1
        int determinar_sexo=MetodoAleatorio.generaNumeroAleatorio(0,1);
         
        //Si es 0 es un chico y si es 1 es una chica
        if(determinar_sexo==CHICO){
            nombre=NOMBRESCHICOS[MetodoAleatorio.generaNumeroAleatorio(0,4)];
            sexo='H';
        }else{
            nombre=NOMBRESCHICAS[MetodoAleatorio.generaNumeroAleatorio(0,4)];
            sexo='M';
        }
         
        //Indicamos la disponibilidad
        disponibilidad();
         
    }
    
    //abtracto, las clases hijas deben implementarlo
    public abstract void disponibilidad();
    //Hago los gets y set 
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getNombre() {
        return nombre;
    }
    public char getSexo() {
        return sexo;
    }
 
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
 
    public int getEdad() {
        return edad;
    }
 
    public void setEdad(int edad) {
        this.edad = edad;
    }
 
    public boolean isAsistencia() {
        return asistencia;
    }
    
    public void setAsistencia(boolean asistencia) {
        this.asistencia = asistencia;
    }
     
    
}
